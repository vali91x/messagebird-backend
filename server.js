'use strict'
const Hapi = require('hapi')
const SocketIO = require('socket.io')
const mongoose = require('mongoose')

const accountController = require('./src/controllers/accountController')
const messagesController = require('./src/controllers/messagesController')

// const DEFAULT_HOST = 'localhost'
const MongoDBUrl = 'mongodb://valentin.iuriet:testtest123@ds217002.mlab.com:17002/messages_app'
const port = Number(process.env.PORT || 3000)

const server = Hapi.server({
  port: port,
  routes: { cors: true },
})

server.route({
  method: 'GET',
  path: '/accounts',
  handler: accountController.login
})

server.route({
  method: 'POST',
  path: '/accounts',
  handler: accountController.signup
})

server.route({
  method: 'GET',
  path: '/messages',
  handler: messagesController.getMessages
})

server.route({
  method: 'POST',
  path: '/messages',
  handler: messagesController.sendMessage
})

server.route({
  method: 'POST',
  path: '/messages/webhook',
  handler: messagesController.sendWebhookMessage
})

const io = SocketIO.listen(server.listener)
io.sockets.on('connect', (socket) => {
  socket.on('request', () => {
    io.emit('refresh')
  })
})

const init = async () => {
  await server.start()
  mongoose.connect(MongoDBUrl, {}).then(
    () => {console.log(`Connected to Mongo server`)},
    err => {console.log(err)})
  console.log(`Server running at: ${server.info.uri}`)
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

init()
