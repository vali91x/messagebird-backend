const ShortUniqueId = require('short-unique-id')
const Account = require('../models/account')

const uuid = new ShortUniqueId()

// Log in with phone number -> returns account obj
exports.login = (req, h) => {
  if (!req.query.phoneNumber) {
    return { err: 'phoneNumber is required param' }.code(400)
  }
  let query = Account.find({})
  query.where('phoneNumber', req.query.phoneNumber)
  return query.exec().then((account) => {
    if (!account || account.length === 0) return { err: 'Account not found' }
    return { account: account[0] }
  }).catch((err) => {
    return { err: err }
  })
}

// Sign up with phone number and name-> returns account obj
exports.signup = (req, h) => {
  const AccountData = {
    id: uuid.randomUUID(6),
    name: req.payload.name,
    phoneNumber: req.payload.phoneNumber
  }
  return Account.create(AccountData).then((account) => {
    return { message: 'Account created successfully', account: account }
  }).catch((err) => {
    return { err: err }
  })
}
