const axios = require('axios')
const Account = require('../models/account')
const MESSAGEBIRD_ACCESS_KEY = 'zkkJ2aBXKFd7CEbOH5YKMnPEd'
const MESSAGEBIRD_REST_PATH_MESSAGES = 'https://rest.messagebird.com/messages'
const MESSAGEBIRD_WEBHOOK_PATH = 'https://flows.messagebird.com/flows/0f86bf00-06c1-4087-ac6a-f16ad43e6e98/invoke'

// Get sent/received messages by phone number -> returns messages (phoneNumber, message, type Sent/Received)
exports.getMessages = async (req, h) => {
  if (!req.query.phoneNumber) {
    return { err: 'phoneNumber is required param' }.code(400)
  }
  try {
    let receivedMessagesRequest = await axios({
      method: 'GET',
      url: MESSAGEBIRD_REST_PATH_MESSAGES,
      headers: {
        'Authorization': `AccessKey ${MESSAGEBIRD_ACCESS_KEY}`
      },
      params: {
        'recipient': req.query.phoneNumber
      }
    })
    let receivedMessagesItems = receivedMessagesRequest.data.items
    let receivedMessages = []
    if (receivedMessagesItems) {
      receivedMessages = receivedMessagesItems.map(message => {
        let messageObj = {}
        messageObj.phoneNumber = message.recipients.items[0].recipient
        messageObj.message = message.body
        messageObj.type = 'Received'
        return messageObj
      })
    }
    let sentMessagesRequest = await axios({
      method: 'GET',
      url: MESSAGEBIRD_REST_PATH_MESSAGES,
      headers: {
        'Authorization': `AccessKey ${MESSAGEBIRD_ACCESS_KEY}`
      },
      params: {
        'originator': req.query.phoneNumber
      }
    })
    let sentMessagesItems = sentMessagesRequest.data.items
    let sentMessages = []

    if (sentMessagesItems) {
      sentMessages = sentMessagesItems.map(message => {
        let messageObj = {}
        messageObj.phoneNumber = message.recipients.items[0].recipient
        messageObj.message = message.body
        messageObj.type = message.recipients.items[0].status === 'delivery_failed' ? 'Error' : 'Sent'
        return messageObj
      })
    }
    return [...receivedMessages, ...sentMessages]
  } catch (err) {
    return { err: err }
  }
}

// Send message with phoneNumber, message, originator
exports.sendMessage = async (req, h) => {
  try {
    let response = await axios({
      method: 'POST',
      url: MESSAGEBIRD_REST_PATH_MESSAGES,
      headers: {
        'Authorization': `AccessKey ${MESSAGEBIRD_ACCESS_KEY}`
      },
      data: {
        originator: req.payload.originator,
        recipients: req.payload.phoneNumber,
        body: req.payload.message
      }
    })
    if (response.data) {
      return {
        phoneNumber: response.data.recipients.items[0].recipient,
        message: response.data.body,
        status: 'Sent'
      }
    }
    return { err: 'Could not send the message' }
  } catch (err) {
    return { err: err }
  }
}

// Send webhook message with Originator, Id (recipient), Message
exports.sendWebhookMessage = async (req, h) => {
  if (!req.payload.message || !req.payload.id || !req.payload.originator) {
    return { err: 'Invalid request' }.code(400)
  }
  let query = Account.find({})
  query.where('id', req.payload.id)
  try {
    let account = await query.exec()
    if (!account || account.length === 0) {
      return { err: 'Account not found' }
    }
    const phoneNumber = account[0].phoneNumber
    try {
      let response = await axios({
        method: 'POST',
        url: MESSAGEBIRD_WEBHOOK_PATH,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          from: req.payload.originator,
          number: phoneNumber,
          text: req.payload.message
        }
      })
      if (response.status === 204) {
        return { status: 'Message sent' }
      }
      return { err: 'Could not send the message' }
    } catch (err) {
      return { err: err }
    }
  }
  catch (err) {
    return { err: err }
  }
}
