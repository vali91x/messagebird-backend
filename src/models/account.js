const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountModel = new Schema({
  id: {
    required: true,
    type: String
  },
  name: {
    required: true,
    type: String
  },
  phoneNumber: {
    required: true,
    type: String
  }
})

module.exports = mongoose.model('Account', AccountModel)
